<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Http\Request;


class HellowWorl extends Component
{
  public $name = "Pallab";
  public $loud = false;
  public $greetings = ['Hello'];

  public function setName($name){
    $this->name = $name; 
  }

  // Alternative for __construct 
  public function mount(Request $request){
    $this->name = $request->input('name');
  }

  public function updated(){
    $this->name = 'updated By Server';
  }
  
  public function render()
    {
        return view('livewire.hellow-worl');
    }
}
