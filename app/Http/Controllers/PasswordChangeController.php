<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PasswordChangeController extends Controller
{
    public function __invoke()
    {
        return view('auth.password-change');
    }
}
