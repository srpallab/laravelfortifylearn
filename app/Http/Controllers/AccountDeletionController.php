<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AccountDeletionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['password.confirm']);
    }
    // view
    public function index()
    {
        return view('auth.delete');
    }
    // delete account
    public function destory()
    {
        // dd('delete');
        auth()->user()->delete();
        return redirect()->route('home');
    }
}
