<x-app-layout>
  @if('password-updated' === session('status'))
    <p> Your Password is updated. </p>
  @endif
  
  <form action="{{route('user-password.update')}}" method="post">
    @csrf
    @method('PUT')
    <div>
      <label for="current_password">Password</label>
      <input name="current_password" type="password" id="current_password"/>
      @if ($errors->updatePassword->any())
	@foreach ($errors->updatePassword->get('current_password') as $error)
	<p>{{ $error }}</p>
	@endforeach
      @endif
    </div>
    <div>
      <label for="password">Password</label>
      <input name="password" type="password" id="password"/>
      @if ($errors->updatePassword->any())
	@foreach ($errors->updatePassword->get('password') as $error)
	  <p>{{ $error }}</p>
	@endforeach
      @endif
    </div>
    <div>
      <label for="password_confirmation">Password Confirmation</label>
      <input name="password_confirmation"
	     type="password" id="password_confirmation"/>
    </div>
    <button type="submit">Change Password</button>
  </form>
</x-app-layout>
