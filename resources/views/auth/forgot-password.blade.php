<x-app-layout>
  <form action="{{route('password.request')}}" method="post">
    @csrf
    <div>
      <label for="email">Email</label>
      <input name="email" type="email" id="email"/>
      @error('email')
      <div>{{$message}}</div>
      @enderror
    </div>
    <button type="submit">Request for New Password</button>
  </form>
</x-app-layout>
