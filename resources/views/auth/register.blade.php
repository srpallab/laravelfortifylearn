<x-app-layout>
  <form action="{{route('register')}}" method="post">
    @csrf
    <div>
      <label for="name">Full Name</label>
      <input name="name" type="text" id="name" />
      @error('name')
      <div>{{$message}}</div>
      @enderror
    </div>
    <div>
      <label for="username">User Name</label>
      <input name="username" type="text" id="username" />
      @error('name')
      <div>{{$message}}</div>
      @enderror
    </div>
    <div>
      <label for="email">Email</label>
      <input name="email" type="email" id="email"/>
      @error('email')
      <div>{{$message}}</div>
      @enderror
    </div>
    <div>
      <label for="password">Password</label>
      <input name="password" type="password" id="password"/>
      @error('password')
      <div>{{$message}}</div>
      @enderror
    </div>
    <div>
      <label for="password_confirmation">Password Confirmation</label>
      <input name="password_confirmation"
	     type="password" id="password_confirmation"/>
      @error('password_confirmation')
      <div>{{$message}}</div>
      @enderror
    </div>
    <button type="submit">Register</button>
  </form>
</x-app-layout>
