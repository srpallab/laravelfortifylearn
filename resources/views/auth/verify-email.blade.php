<x-app-layout>
  <h1>Please Verify Your Email.</h1>
  @if (session('status') === 'verification-link-send' )
    <p>A verifiaction email has been send.</p>  
  @endif
  <form method="{{ route('verification.send') }}" action="post">
    @csrf
    <button type="submit">Reset Email</button>
  </form>
</x-app-layout>
