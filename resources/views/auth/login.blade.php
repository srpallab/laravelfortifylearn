<x-app-layout>
  <form action="{{route('login')}}" method="post">
    @csrf
    <div>
      <label for="email">Email or UserName</label>
      <input name="email" type="text" id="email"/>
      @error('email')
      <div>{{$message}}</div>
      @enderror
    </div>
    <div>
      <label for="password">Password</label>
      <input name="password" type="password" id="password"/>
      @error('password')
      <div>{{$message}}</div>
      @enderror
    </div>
    <div>
      <input name="remember" type="checkbox" id="remember"/>
      <label for="remember">Remember Me</label>
    </div>
    <div>
      <a href="{{route('password.request')}}">Forgotten Your Password?</a>
    </div>
    <button type="submit">Login</button>
  </form>
</x-app-layout>
