<x-app-layout>
  <form action="{{route('password.update')}}" method="post">
    @csrf
    <input name="email" type="hidden" value="{{request()->email}}"/>
    <input name="token" type="hidden" value="{{request()->token}}"/>
    <div>
      <label for="password">Password</label>
      <input name="password" type="password" id="password"/>
      @error('password')
      <div>{{$message}}</div>
      @enderror
    </div>
    <div>
      <label for="password_confirmation">Password Confirmation</label>
      <input name="password_confirmation"
	     type="password" id="password_confirmation"/>
      @error('password_confirmation')
      <div>{{$message}}</div>
      @enderror
    </div>
    <button type="submit">Change Password</button>
  </form>
</x-app-layout>
