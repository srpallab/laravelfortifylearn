<x-app-layout>
  <form action="/two-factor-challenge" method="post">
    @csrf
    <div>
      <label for="code">Two Factor Code</label>
      <input name="code" type="password" id="code"/>
      @error('code')
      <div>{{$message}}</div>
      @enderror
    </div>
    <div>
      <p>OR</p>
    </div>
    <div>
      <label for="recovery_code">Recovery Code</label>
      <input name="recovery_code" type="password" id="recovery_code"/>
      @error('recovery_code')
      <div>{{$message}}</div>
      @enderror
    </div>
    <button type="submit">Continue</button>
  </form>
</x-app-layout>
