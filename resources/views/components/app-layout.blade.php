<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <title>Fortify</title>
    @livewireStyles
  </head>
  <body>
    <ul>
      @guest
      <li><a href="{{ route('login') }}">Login</a></li>
      <li><a href="{{ route('register') }}">Register</a></li>
      @endguest
      @auth
      <li><a href="{{ route('profile') }}">{{ auth()->user()->name }}</a></li>
      <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
      <li><a href="{{ route('auth.delete') }}">Delete Account</a></li>
      <li><a href="{{ route('auth.password-change') }}">Change Password</a></li>
      <li><a href="{{ route('auth.twofactor') }}">Two Factor</a></li>
      <li>
	<form action="{{ route('logout') }}" method="post">
	  @csrf
	  <button type="submit">Logout</button>
	</form>
      </li>
      @endauth
    </ul>
    {{  $slot }}
    @livewireScripts
  </body>
</html>
