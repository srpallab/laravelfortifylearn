<x-app-layout>
  <form action="{{route('user-profile-information.update')}}" method="post">
    @csrf
    @method('PUT')
    <div>
      <label for="name">Full Name</label>
      <input name="name" type="text" id="name" value="{{auth()->user()->name}}" />
      @error('name')
      <div>{{$message}}</div>
      @enderror
    </div>
    <div>
      <label for="username">User Name</label>
      <input name="username" type="text" id="username" value="{{auth()->user()->username}}" />
      @error('username')
      <div>{{$message}}</div>
      @enderror
    </div>
    <div>
      <label for="email">Email</label>
      <input name="email" type="email" id="email" value="{{auth()->user()->email}}" />
      @error('email')
      <div>{{$message}}</div>
      @enderror
    </div>
    <button type="submit">update</button>
  </form>
</x-app-layout>
