<div>
  <input type="text" name="name" wire:model="name" />
  <input type="checkbox" wire:model="loud" />
  <select wire:model="greetings" multiple>
    <option>Hello</option>
    <option>Goodbye</option>
    <option>Adios</option>
  </select>
  <form action="#" wire:submit.prevent="setName('Bingo')">
    {{ implode(', ', $greetings) }} World {{ $name }} @if ($loud) ! @endif
    <button>Reset Name</button>
  </form>
</div>
